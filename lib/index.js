"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Nav = require("./Components/Nav");

Object.defineProperty(exports, "Nav", {
  enumerable: true,
  get: function get() {
    return _Nav.Nav;
  }
});

var _Logo = require("./Components/Logo");

Object.defineProperty(exports, "Logo", {
  enumerable: true,
  get: function get() {
    return _Logo.Logo;
  }
});