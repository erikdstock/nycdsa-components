"use strict";

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _react3 = require("@storybook/react");

var _Nav = require("../Components/Nav");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(0, _react3.storiesOf)("Nav", module).add("Default", function () {
  return _react2.default.createElement(_Nav.Nav, null);
});
// import { action } from "@storybook/addon-actions"
// import { linkTo } from "@storybook/addon-links"