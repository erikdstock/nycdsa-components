"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Link = undefined;

var _templateObject = _taggedTemplateLiteral(["\n  text-decoration: none;\n  color: ", ";\n  font-family: Helvetica;\n  text-transform: uppercase;\n  font-size: 12px;\n  padding-bottom: 3px;\n  &:focus,\n  &:hover {\n    color: ", ";\n    border-bottom: 2px solid ", ";\n  }\n  &.active {\n    color: ", ";\n    border-bottom: 2px solid ", ";\n  }\n"], ["\n  text-decoration: none;\n  color: ", ";\n  font-family: Helvetica;\n  text-transform: uppercase;\n  font-size: 12px;\n  padding-bottom: 3px;\n  &:focus,\n  &:hover {\n    color: ", ";\n    border-bottom: 2px solid ", ";\n  }\n  &.active {\n    color: ", ";\n    border-bottom: 2px solid ", ";\n  }\n"]);

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _styledComponents = require("styled-components");

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _theme = require("../theme");

var _reactRouterDom = require("react-router-dom");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Link = exports.Link = (0, _styledComponents2.default)(_reactRouterDom.NavLink)(_templateObject, _theme.colors.inactiveGray, _theme.colors.uiGray, _theme.colors.inactiveRed, _theme.colors.uiGray, _theme.colors.red);