"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Link = exports.Nav = undefined;

var _templateObject = _taggedTemplateLiteral(["\n  padding-top: 15px;\n  height: 108px;\n  background: ", ";\n  align-items: center;\n"], ["\n  padding-top: 15px;\n  height: 108px;\n  background: ", ";\n  align-items: center;\n"]),
    _templateObject2 = _taggedTemplateLiteral(["\n  display: flex;\n  justify-content: space-between;\n  max-width: 1200px;\n  width: 90%;\n  min-width: 768px;\n  height: 100%;\n  padding: 5px;\n  margin: 0 auto;\n"], ["\n  display: flex;\n  justify-content: space-between;\n  max-width: 1200px;\n  width: 90%;\n  min-width: 768px;\n  height: 100%;\n  padding: 5px;\n  margin: 0 auto;\n"]),
    _templateObject3 = _taggedTemplateLiteral(["\n  text-decoration: none;\n  color: ", ";\n  font-family: Helvetica;\n  text-transform: uppercase;\n  font-size: 12px;\n  padding-bottom: 3px;\n  &:focus,\n  &:hover {\n    color: ", ";\n    border-bottom: 2px solid ", ";\n  }\n  &.active {\n    color: ", ";\n    border-bottom: 2px solid ", ";\n  }\n"], ["\n  text-decoration: none;\n  color: ", ";\n  font-family: Helvetica;\n  text-transform: uppercase;\n  font-size: 12px;\n  padding-bottom: 3px;\n  &:focus,\n  &:hover {\n    color: ", ";\n    border-bottom: 2px solid ", ";\n  }\n  &.active {\n    color: ", ";\n    border-bottom: 2px solid ", ";\n  }\n"]),
    _templateObject4 = _taggedTemplateLiteral(["\n  display: flex;\n  width: 500px;\n  justify-content: space-between;\n  margin-left: auto;\n  height: 20px;\n  align-self: center;\n"], ["\n  display: flex;\n  width: 500px;\n  justify-content: space-between;\n  margin-left: auto;\n  height: 20px;\n  align-self: center;\n"]);

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _styledComponents = require("styled-components");

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _theme = require("../theme");

var _Logo = require("./Logo");

var _reactRouterDom = require("react-router-dom");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Nav = exports.Nav = function Nav() {
  return _react2.default.createElement(
    Header,
    null,
    _react2.default.createElement(
      Banner,
      null,
      _react2.default.createElement(_Logo.Logo, { height: "100%" }),
      _react2.default.createElement(
        StyledNav,
        { cool: "woot" },
        _react2.default.createElement(
          Link,
          { activeClassName: "active", to: "/membership" },
          "Membership"
        ),
        _react2.default.createElement(
          Link,
          { activeClassName: "active", to: "/events" },
          "Events"
        ),
        _react2.default.createElement(
          Link,
          { activeClassName: "active", to: "/working-groups" },
          "Working Groups"
        ),
        _react2.default.createElement(
          Link,
          { activeClassName: "active", to: "/branches" },
          "Branches"
        ),
        _react2.default.createElement(
          Link,
          { activeClassName: "active", to: "/donate" },
          "Donate"
        )
      )
    )
  );
};

var Header = _styledComponents2.default.header(_templateObject, _theme.colors.white);

var Banner = _styledComponents2.default.div(_templateObject2);

var Link = exports.Link = (0, _styledComponents2.default)(_reactRouterDom.NavLink)(_templateObject3, _theme.colors.inactiveGray, _theme.colors.uiGray, _theme.colors.inactiveRed, _theme.colors.uiGray, _theme.colors.red);

var StyledNav = _styledComponents2.default.nav(_templateObject4);