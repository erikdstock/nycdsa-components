import React from "react"
import styled from "styled-components"
import { storiesOf } from "@storybook/react"
// import { action } from "@storybook/addon-actions"
// import { linkTo } from "@storybook/addon-links"

import { CalendarIcon, LocationIcon } from "../Components/Icons"
import { colors } from "../theme"

const Wrapper = styled.div`
  width: ${p => p.width || 200}px;
  height: ${p => p.height || 100}px;
  background: ${p => p.color || colors.white};
  padding: 8px;
  display: flex;
  justify-content: center;
  align-items: center;
  border: 1px solid ${colors.inactiveGray};
  border-radius: 4px;
  margin: 5px;
`

const Container = styled.div`
  display: flex;
`

storiesOf("Icons", module)
  .add("Gallery", () => (
    <Container>
      <Wrapper>
        <CalendarIcon /> Occupie Wall Street
      </Wrapper>
      <Wrapper>
        <LocationIcon /> Biscotti Park
      </Wrapper>
    </Container>
  ))
  .add("Big", () => (
    <Container>
      <Wrapper>
        <CalendarIcon /> Occupie Wall Street
      </Wrapper>
      <Wrapper>
        <LocationIcon /> Biscotti Park
      </Wrapper>
    </Container>
  ))
