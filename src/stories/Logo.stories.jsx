import React from "react"
import styled from "styled-components"
import { storiesOf } from "@storybook/react"
// import { action } from "@storybook/addon-actions"
// import { linkTo } from "@storybook/addon-links"

import { Logo } from "../Components/Logo"
import { colors } from "../theme"

const Container = styled.div`
  width: ${p => p.width}px;
  height: ${p => p.height}px;
  background: ${p => p.color || colors.backgroundGray};
  padding: 8px;
`

storiesOf("Logo", module)
  .add("Default", () => <Logo />)
  .add("Black", () => <Logo color={colors.black} />)
  .add("white on red", () => (
    <Container color={colors.red} width={180} height={180}>
      <Logo color={colors.white} />
    </Container>
  ))
  .add("white on black", () => (
    <Container color={colors.black} width={180} height={180}>
      <Logo color={colors.white} />
    </Container>
  ))
  .add("in a narrow container", () => (
    <Container width={90} height={180}>
      <Logo />
    </Container>
  ))
  .add("in a short container", () => (
    <Container width={180} height={90}>
      <Logo height="100%" />
    </Container>
  ))
  .add("in a big container", () => (
    <Container width={1000} height={1000}>
      <Logo />
    </Container>
  ))
