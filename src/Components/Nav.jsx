import React from "react"
import styled from "styled-components"
import { colors } from "../theme"
import { Logo } from "./Logo"
import { NavLink } from "react-router-dom"

export const Nav = () => (
  <Header>
    <Banner>
      <Logo height="100%" />
      <StyledNav cool="woot">
        <Link activeClassName="active" to="/membership">
          Membership
        </Link>
        <Link activeClassName="active" to="/events">
          Events
        </Link>
        <Link activeClassName="active" to="/working-groups">
          Working Groups
        </Link>
        <Link activeClassName="active" to="/branches">
          Branches
        </Link>
        <Link activeClassName="active" to="/donate">
          Donate
        </Link>
      </StyledNav>
    </Banner>
  </Header>
)

const Header = styled.header`
  padding-top: 15px;
  height: 108px;
  background: ${colors.white};
  align-items: center;
`

const Banner = styled.div`
  display: flex;
  justify-content: space-between;
  max-width: 1200px;
  width: 90%;
  min-width: 768px;
  height: 100%;
  padding: 5px;
  margin: 0 auto;
`

export const Link = styled(NavLink)`
  text-decoration: none;
  color: ${colors.inactiveGray};
  font-family: Helvetica;
  text-transform: uppercase;
  font-size: 12px;
  padding-bottom: 3px;
  &:focus,
  &:hover {
    color: ${colors.uiGray};
    border-bottom: 2px solid ${colors.inactiveRed};
  }
  &.active {
    color: ${colors.uiGray};
    border-bottom: 2px solid ${colors.red};
  }
`

const StyledNav = styled.nav`
  display: flex;
  width: 500px;
  justify-content: space-between;
  margin-left: auto;
  height: 20px;
  align-self: center;
`
