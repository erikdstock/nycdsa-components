import React from "react"
import PropTypes from "prop-types"
import { colors } from "../../theme"

export const LocationIcon = props => (
  <svg viewBox="0 0 16 20" width="1em" height="1em" {...props}>
    <path
      d="M15 8c0 3.1-4.5 8.576-6.284 10.625a.942.942 0 0 1-1.432 0C5.501 16.576 1 11.1 1 8a7 7 0 0 1 14 0z"
      stroke={props.color}
      fill="none"
    />
  </svg>
)
LocationIcon.propTypes = {
  color: PropTypes.string
  // height: PropTypes.string
}

LocationIcon.defaultProps = {
  color: colors.red
  // height: "100%"
}
