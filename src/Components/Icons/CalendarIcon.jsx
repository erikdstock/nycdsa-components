import React from "react"
import PropTypes from "prop-types"
import { colors } from "../../theme"

export const CalendarIcon = props => (
  <svg viewBox="0 0 20 20" width="1em" height="1em" {...props}>
    <g fill="none">
      <mask id="a" fill="#fff">
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M0 2a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v16a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2z"
        />
      </mask>
      <g mask="url(#a)">
        <path
          d="M2 1h16v-2H2v2zm17 1v16h2V2h-2zm-1 17H2v2h16v-2zM1 18V2h-2v16h2zm1 1a1 1 0 0 1-1-1h-2a3 3 0 0 0 3 3v-2zm17-1a1 1 0 0 1-1 1v2a3 3 0 0 0 3-3h-2zM18 1a1 1 0 0 1 1 1h2a3 3 0 0 0-3-3v2zM2-1a3 3 0 0 0-3 3h2a1 1 0 0 1 1-1v-2z"
          fill={props.color}
        />
      </g>
      <path d="M1 5.5h18" stroke={props.color} />
    </g>
  </svg>
)

CalendarIcon.propTypes = {
  color: PropTypes.string
  // height: PropTypes.string
}

CalendarIcon.defaultProps = {
  color: colors.red
  // height: "100%"
}
