// theme.js

/*
/** From ROSA DSA Style Guide (April 2018)
/ https://www.figma.com/file/bOVOFrh7h2aIPpHAmFCoD4rl/ROSA?node-id=0%3A453
/ Not currently setting opacity on inactive colors because... I don't know how

$bold-red: #E8220C;
$comrade-red: #f2390b;
$inactive-red: #f18575;
$ui-gray: #444444;
$inactive-gray: #8f8f8f;
$stroke-gray: #e7e7e7;
$background-gray: #f2f2f2;
$white: #ffffff;

// assistant font
@import url('https://fonts.googleapis.com/css?family=Assistant:400,700');
////////////
*/

// breakpoint values
// any array length works with styled-system
export const breakpoints = ["40em", "52em", "64em"]

export const colors = {
  text: "#024",
  blue: "#07c",
  black: "#000000",

  // from figma/website mocks
  red: "#EC1F27",

  // from rosa
  inactiveRed: "#f18575",
  uiGray: "#444444",
  inactiveGray: "#8f8f8f",
  strokeGray: "#e7e7e7",
  backgroundGray: "#f2f2f2",
  white: "#ffffff",

  // nested objects work as well
  dark: {
    blue: "#058"
  },
  // arrays can be used for scales of colors
  gray: ["#333", "#666", "#999", "#ccc", "#eee", "#f6f6f6"]
}

// space is used for margin and padding scales
// it's recommended to use powers of two to ensure alignment
// when used in nested elements
// numbers are converted to px
export const space = [0, 4, 8, 16, 32, 64, 128, 256, 512]

// typographic scale
export const fontSizes = [12, 14, 16, 20, 24, 32, 48, 64, 96, 128]

// for any scale, either array or objects will work
export const lineHeights = [1, 1.125, 1.25, 1.5]

export const fontWeights = {
  normal: 500,
  bold: 700
}

export const letterSpacings = {
  normal: "normal",
  caps: "0.25em"
}

// border-radius
export const radii = [0, 2, 4, 8]

export const borders = [0, "1px solid", "2px solid"]

export const shadows = [
  `0 1px 2px 0 ${colors.text}`,
  `0 1px 4px 0 ${colors.text}`
]

export const theme = {
  breakpoints,
  colors,
  space,
  fontSizes,
  lineHeights,
  fontWeights,
  letterSpacings,
  radii,
  borders,
  shadows
}
