# Component Library

* [React.js](https://reactjs.org/)
* [storybook.js](https://storybook.js.org/)
* [styled-components](https://www.styled-components.com/)💅
* [styled-system](https://github.com/jxnblk/styled-system)

## Development
The project is configured for easy development in [vscode](https://code.visualstudio.com/) with some recommended packages and defaults for eslint and [prettier](https://prettier.io/)

### In Storybook

_requires modern node.js + yarn_

* `yarn install`
* `yarn storybook`
  _storybook available at http://localhost:6006/_

### As a local package alonside a consumer app

* `yarn install`
* `yarn link` - This step would actually only be necessary with a *published* npm package

  _Run the babel transpiler, watching for changes_
* `yarn watch`

_In your other app:_
* Add dependencies to package.json (using a **relative file path** because this is not yet published):

```json
    "nycdsa-components": "file:../nycdsa-components",
    "react": "^16.3.2",
    "react-dom": "^16.3.2",
    "react-router-dom": "^4.2.2",
    "styled-components": "^3.2.6",
    "styled-system": "^2.2.4"
```
* `yarn link nycdsa-components` - again, not necessary since we are referencing a local file, not a published package 
* `yarn install`
* `yarn start`

The following example can be dropped into create-react-app's App.jsx:

```jsx
import React from "react"
import { Nav } from "nycdsa-components"
import { BrowserRouter } from "react-router-dom"

class App extends React.Component {
  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <Nav />
          <article
            style={{
              textAlign: "left",
              width: "768px",
              margin: "30px auto",
              borderTop: "1px solid grey"
            }}
          >
            <p>Welcome</p>
          </article>
        </div>
      </BrowserRouter>
    )
  }
}

export default App
```

